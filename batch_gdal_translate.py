#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Batch gdal_tranlate to extract an AOI define with -projwin

Auteur: Loïc Lozac'h
"""

import os, argparse
from subprocess import Popen, PIPE

def search_files(directory='.', matchnaming='S1',extension='TIF',fictype='f'):
    images=[]
    matchnaming = matchnaming.upper()
    extension = extension.upper()
    fictype = fictype.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype != 'd':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if  name.upper().find(matchnaming) >= 0 and name.upper().endswith(extension):
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if dirname.upper().find(matchnaming) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit
            
    return images

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    



if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Batch gdal_tranlate to extract an AOI define with -projwin
        """)
    
    parser.add_argument('-srcdir', action='store', required=True, help='Directory containing geotiff to process')
    parser.add_argument('-projwin', action='store', required=True, help='Window extend in coordinates AND COMA SEPARATED, ex: -projwin ulx,uly,lrx,lry')
    parser.add_argument('-outdir', action='store', required=True, help='Output directory')
    
    args=parser.parse_args()
    
    mvfiles=[]
    mvfiles=search_files(args.srcdir)
    
    projwin=args.projwin.split(",")
    if len(projwin) != 4:
        print("Error: winproj argument must have 4 elements")
        exit()
    
    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)

    cmdbase=["gdal_translate","-eco","-winproj",projwin[0],projwin[1],projwin[2],projwin[3]]
    
    for file in mvfiles :
        destfile = os.path.join(args.outdir,os.path.basename(file)[:-4]+"_Subset.tif")
        cmd=[]
        cmd = [c for c in cmdbase]
        cmd.append(file)
        cmd.append(destfile)
        process_command(cmd)
        
            
        
        
            
    
    




