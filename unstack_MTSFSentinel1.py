#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Unstack and converts to geotiff MTFS processed Sentinel-1

Auteur: Loïc Lozac'h
"""

import xml.etree.ElementTree as ET
import os, argparse, shutil
from subprocess import Popen, PIPE
from osgeo import gdal

def search_files(directory='.', resolution='S1', extension='dim', fictype='f'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit
            
    return images


def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")

if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Unstack, converts to geotiff and mosaic MTFS processed Sentinel-1 BEAM-DIMAP stack files
        """)
    
    parser.add_argument('-dimdir', action='store', required=True, help='Directory containing BEAM-DIMAP stack files to be processed (.dim)')
    parser.add_argument('-outdir', action='store', required=True, help='Output directory')
    
    args=parser.parse_args()
    
    s1dims=search_files(args.dimdir)
    
    if len(s1dims)==0:
        print("No dim file found in directory!")
        exit()
    
    cmdbase = ["gdal_translate"] 
    
    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)
    
    histlist=[]
    tomosa=[]
    i=0
    for s1dim in s1dims:
        
        tree = ET.parse(s1dim)
        root = tree.getroot()

        datadir = s1dim[:-3]+"data"

        abstractnode = root.find(".//MDElem[@name='Abstracted_Metadata']")
        mstproduct = abstractnode.find(".//*[@name='PRODUCT']").text
        mstproductsp = mstproduct.split("_")
    
        slavenode = root.find(".//MDElem[@name='Slave_Metadata']")
        
        
        for child in slavenode:
            
            if child.get("name")=="Master_bands":
                mstbands = child.text.split(" ")
                for b in mstbands:
                    cmd = []
                    i+=1
                    bsp = b.split("_")
                    destfile = os.path.join(args.outdir,"_".join(mstproductsp[:6])+"_"+"_".join(bsp[:2])+".tif")
                    sourcefile = os.path.join(datadir,b+"_db.img")
                    cmd = ["gdal_translate"]
                    cmd.append(sourcefile)
                    cmd.append(destfile)
                    if not os.path.exists(destfile):
                        process_command(cmd)
                    histlist.append(destfile)
                    
                    acqdate = mstproductsp[4][:9]
                    polari = bsp[1]
                    plateform = mstproductsp[0]
                    l = [histlist.index(i) for i in histlist if (plateform in i) and (acqdate in i) and (polari in i)]
                    if len(l) == 2 :
                        tomosa.append((histlist[l[0]],histlist[l[1]]))
            
            else:
                product = child.find(".//*[@name='PRODUCT']").text
                productsp = product.split("_")
                bands = child.find(".//*[@name='Slave_bands']").text.split(" ")
                for b in bands:
                    cmd = []
                    i+=1
                    bsp = b.split("_")
                    destfile = os.path.join(args.outdir,"_".join(productsp[:6])+"_"+"_".join(bsp[:2])+".tif")
                    sourcefile = os.path.join(datadir,b+"_db.img")
                    cmd = ["gdal_translate"]
                    cmd.append(sourcefile)
                    cmd.append(destfile)
                    
                    if not os.path.exists(destfile):
                        process_command(cmd)
                    
                    histlist.append(destfile)
                    acqdate = productsp[4][:9]
                    polari = bsp[1]
                    plateform = productsp[0]
                    l = [histlist.index(i) for i in histlist if (plateform in i) and (acqdate in i) and (polari in i)]
                    if len(l) == 2 :
                        tomosa.append((histlist[l[0]],histlist[l[1]]))
                    
            
            print("\nNumber of bands processed: "+str(i)+"\n")
        
    print("Mosaicing...\n")
    
    tempdir = os.path.join(args.outdir,"temp")
    if not os.path.exists(tempdir):
        os.mkdir(tempdir)
        
    for mos in tomosa:
        
        tempmos0 = os.path.join(tempdir,os.path.basename(mos[0]))
        tempmos1 = os.path.join(tempdir,os.path.basename(mos[1]))
        shutil.move(mos[0], tempmos0)
        shutil.move(mos[1], tempmos1)
        outmos = mos[0]
        cmd = ["gdal_merge.py","-n","0","-a_nodata","0","-o",outmos,tempmos0,tempmos1]
        print("\nOutfile: "+outmos)
        process_command(cmd)
            
            
    
        
            
    
    



