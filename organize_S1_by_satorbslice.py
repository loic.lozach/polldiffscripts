#! /usr/bin/python3
# -*- coding: utf-8 -*-
"""
Script permettant de trier les images Sentinel-1 en fonction de leur plateforme,
    de leur orbite relative et du slice number.

Auteur: Loïc Lozac'h
"""

import xml.etree.ElementTree as ET
import os, shutil, argparse


def search_files(directory='.', matchnaming='S1',extension='dim',fictype='f'):
    images=[]
    matchnaming = matchnaming.upper()
    extension = extension.upper()
    fictype = fictype.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype != 'd':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if  name.upper().find(matchnaming) >= 0 and name.upper().endswith(extension):
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if dirname.upper().find(matchnaming) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit
            
    return images



if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Organize S1 calibrated files by satellite, relative orbit and slice for multi-temporal speckle filtering
        """)
    
    parser.add_argument('-s1dir', action='store', required=True, help='Directory containing Calibrated Sentinel1 in DIMAP-BEAM format')
    
    args=parser.parse_args()
    
    s1dim=[]
    s1dim=search_files(args.s1dir)
    metas1=[]
#     dirsel=[]
    for file in s1dim :
        #---metadata parsing
        tree = ET.parse(file)
        root = tree.getroot()
                
        #---metadata profile
        balises = root.findall('Dataset_Sources/MDElem/MDElem/MDATTR')
        metab={}
        for balise in balises :
            if balise.attrib['name'] == 'PRODUCT':
                metab.setdefault('PRODUCT',balise.text)
            elif balise.attrib['name'] == 'MISSION':
                metab.setdefault('MISSION',"S"+balise.text[-2:])
            elif balise.attrib['name'] == 'PASS':
                metab.setdefault('PASS',balise.text[:3])
            elif balise.attrib['name'] == 'REL_ORBIT':
                metab.setdefault('REL_ORBIT',balise.text)
            elif balise.attrib['name'] == 'slice_num':
                metab.setdefault('slice_num',balise.text)
        
        outputdir = os.path.join(args.s1dir,metab['MISSION']+"_"+"ORB"+metab['REL_ORBIT']+"_"+"SL"+metab['slice_num'])
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)

        #move
        print("copy : "+file[:-3]+"*")
        print("to   : "+outputdir+"\n")
        
        shutil.move(file,outputdir)
        shutil.move(file[:-3]+"data",outputdir)
    
            
    
    




